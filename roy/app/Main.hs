{-# LANGUAGE NoImplicitPrelude #-} -- for RIO
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-type-defaults #-} -- for RIO ??
{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -Wcompat #-}
{-# OPTIONS_GHC -Wincomplete-record-updates #-}
{-# OPTIONS_GHC -Wincomplete-uni-patterns #-}
{-# OPTIONS_GHC -Wredundant-constraints #-}

module Main where

import Options.Applicative
-- import Data.Semigroup ((<>))

import RIO
import qualified System.IO as SI
import RIO.Process
import           System.Directory (doesFileExist)
-- import           System.Environment
-- import System.Exit
-- import           System.Lock.FLock
import qualified RIO.Text as T
import qualified Data.Text.IO as TI
default (T.Text)

-- read a character with a timeout default character if empty answer
readChar :: Integer -> String -> Char -> IO (Maybe Char)
readChar seconds prompt defchar = do
  let millis = seconds * 10 ^ (6 :: Integer)
  SI.putStr prompt
  ans <- timeout (fromIntegral millis) SI.getChar
  case ans of
    Just '\n' -> return $ Just defchar
    _ -> return ans

  
data CliOpts = CliOpts
  { stateFile   :: Text
  , asgname :: Text
  , auto     :: Bool
  , dryrun   :: Bool
  }
  deriving (Show)
  
cliOpts :: ParserInfo CliOpts
cliOpts = info (opts <**> helper)
      ( fullDesc
     <> progDesc "Just playing around"
     <> header "roy - a testbed" )
  where
    opts = CliOpts
        <$> strArgument
            ( metavar "FILE"
           <> help "Filename for this program's state" )
        <*> strArgument
            ( metavar "NAME"
           <> help "Name of ASG" )
        <*> switch
            ( long "auto"
           <> short 'a'
           <> showDefault
           <> help "Auto proceed" )
        <*> switch
            ( long "dryrun"
           <> short 'd'
           <> showDefault
           <> help "dryrun" )

-- Here comes the RIO boilerplate! (see tutorial)
data App = App
  { appLogFunc :: !LogFunc
  , appProcessContext :: !ProcessContext
  }
instance HasLogFunc App where
  logFuncL = lens appLogFunc (\x y -> x { appLogFunc = y })
instance HasProcessContext App where
  processContextL = lens appProcessContext (\x y -> x { appProcessContext = y })
-- end of RIO biolerplate

main :: IO ()
main =  do
  -- more RIO boilerplate, could use runSimpleApp instead
  lo <- logOptionsHandle stderr True
  pc <- mkDefaultProcessContext
  withLogFunc lo $ \lf ->
    let app = App
          { appLogFunc = lf
          , appProcessContext = pc
          }
     in runRIO app run

run :: RIO App ()
run = do
  --  hSetBuffering stdout LineBuffering
  opts <-liftIO $ execParser cliOpts
  logInfo $ displayShow opts
  let sfile = stateFile opts
  initState sfile (asgname opts)
  rrun_
  asg <- getStateText sfile "asg"
  logInfo $ displayShow asg
  logInfo "bye"
--  stateFile <- sfile opts
--  initState stateFile (asgname opts)
  -- shelly $ verbosely $ do
  --   host <- run "uname" ["-n"]
  --   echo host
  --   inspect $ sfile opts

getStateText :: Text -> Text -> RIO App (Maybe Text)
getStateText sfile key = do
  (ret, ans) <-proc "grep" [T.unpack key, T.unpack sfile] readProcessStdout
  case ret of
    ExitSuccess -> return $ Just $ RIO.tshow ans
    ExitFailure ec -> do
      logDebug $ displayShow $
        T.intercalate " "  ["grep", key, sfile,
                             "failed with exit code:", RIO.tshow ec
                           ]
      return Nothing

rrun_ :: RIO App ()
rrun_ = do
  proc "echo" ["foobar"] runProcess_

updateOrAddLine :: Text -> Text -> [Text] -> [Text]
updateOrAddLine key valu ls = loop (sol key) (entry key valu) ls
  where
    sol :: Text -> Text
    sol k = T.append k ": "
    entry :: Text -> Text -> Text
    entry k v = T.append (sol k) v
    -- replace k with v or append to ls
    loop :: Text -> Text -> [Text] -> [Text]
    loop k v (ll:lls) = if k `T.isPrefixOf` ll
                        then v : loop k "" lls
                        else ll : loop k v lls
    loop _ "" [] = []
    loop _ v [] = [v]
    
updateState :: Text -> Text -> Text -> IO ()
updateState sfile key valu = do
  let fname = show sfile
  contents <- TI.readFile fname
  let ls' = T.lines contents
  let ls = updateOrAddLine key valu ls'
  TI.writeFile fname $ T.unlines ls'

-- readLs path = readFile sfile >>= lines

initState :: Text -> Text -> RIO App ()
initState sfile asg = do
  let fname = T.unpack sfile
  new <- liftIO $ doesFileExist fname
  if new
    then
      logError "Found state file, not recreating"
    else
    do
      liftIO $ TI.writeFile fname "# File start"
      liftIO $ updateState sfile  "asg" asg

