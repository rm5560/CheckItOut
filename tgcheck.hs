#! /usr/bin/env stack
{- stack
   script
   --resolver lts-18.24
   --package hspec
   --package hspec-contrib
   --package megaparsec
   --package text
   --package containers
-}

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module TgCheck where

import Test.Hspec
-- import Test.QuickCheck
-- import Control.Exception (evaluate)

import qualified Text.Megaparsec as P
import           Text.Megaparsec.Char
import           Data.Text (Text)
import           Data.Void
-- import           Data.Set (Set)
-- import qualified Data.Set as Set

import           Control.Applicative
import           Control.Monad
import qualified Data.Text.IO  as I
import           Text.Megaparsec hiding (State)
import qualified Data.Text as T
-- import qualified Text.Megaparsec.Char.Lexer as L

type Parser = Parsec Void Text

readTgHcl:: IO Text
readTgHcl = I.readFile "terragrunt.hcl" 

main = do
     tg <- readTgHcl
     case parse pTf "" tg of
        Left bundle -> putStr (errorBundlePretty bundle)
        Right txt -> hspec $ do
	  describe "terraform blpck" $ do
            it "has a source declaration" $ do
              pending

data TgHcl = TgHcl
  { terraform     :: TgTerraform
  -- , remote_state  :: TgRemoteState
  -- , include :: TgInclude
  -- , locals  :: TgLocals
  -- , dependency :: TgDependency
  -- , dependencies :: TgDependencies
  -- , generate :: TgGenerate
  }

data TgTerraform = TgTerraform
  { source :: Maybe TgSource
  }
  
pTgTerraform :: Parser TgTerraform
pTgTerraform = do
  space
  tg <- string "terraform"
  space
  char '{'
  space
  (Assign name value) <- pAssign
  if name == "source"
    then return $ TgTerraform { source = (Just $ LocalPath value) }
    else fail "No Source"

data TgSource = LocalPath Text
  | GitHub GhSource

data GhSource = GhSource
  { ghSurl    :: Text
  , ghSorg    :: Text
  , ghSmodule :: Text
  , ghSrepo   :: Text
  , ghSref    :: Maybe Text
  }
pGhSource = do
  string "git@github.com:"
  org <- parseTillChar '/'
  repo <- parseTillChar  '/
  char '/'
  mod <- parsetill "?"
  string "ref="
  ref <- takeWhile1P (Just "ref") eol
  return (Source org, repo, mod, (Just ref))

parseWhile :: Parse Char -> Parse Text
parseWhile =  T.pack <$> P.many

parseTillChar :: Char -> Parse Text
parseTillChar c = parseWhile (anySingleBut c)

data Assign =  Assign
  { name :: Text
  , value :: Text
  } deriving (Eq, Show)
     
pAssign :: Parser Assign
pAssign = do
   space
   name <- parsewhile (alphaNumChar <|> char '_')
   space
   char '='
   space
   char '"'
   value <- T.pack <$> P.many (anySingleBut '"')
   char '"'
   return (Assign name value)
